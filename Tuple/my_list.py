arr_student = ("Student A", "Student B", "Student C")
print(arr_student)
print("===============")
print("First student is: ", arr_student[0])
print("======show by loop =======")
for i in arr_student: 
    print(i)

print("==========================")
c = len(arr_student)
for i in range(0, c):
    print(arr_student[i])