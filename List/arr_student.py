arr_stu = ["Reach", "Youei", \
    "Sokha", "Sambath", \
        "Chantha", "Chea", \
            "Kampuchea", "Sophea", "Sokchea"]
# Edit List item 0
arr_stu[0] = "Reach chea"
print(arr_stu)
# Add new item with append() method
# arr_stu.append([1, 2, 3, 4])
arr_stu.append("Student ADD")
print(arr_stu)
# Order item 
arr_stu.sort(reverse=True)
print(arr_stu)


# for i in arr_stu: 
#     print(i)