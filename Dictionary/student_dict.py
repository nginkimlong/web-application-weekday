student_dict = { 'A': 'Student A', 'B': 'Student B', 'C': 'Student C', 
    'E': 'Student E', 
    'F': 'Student F'
}
# Edit dictionary item
student_dict['A'] = "New Student A"
print(student_dict)

# Delete dictionary item
del student_dict['A']
print(student_dict)

# Find student 
f1 = student_dict['B']
print("Find one: ", f1)
f2 = student_dict.get('C')
print("Find two: ", f2)
# Add new value to dictionary student 
student_dict['G'] = "Student G"
print(student_dict)
# Get key & value 
k  = student_dict.keys()
print(k)
v = student_dict.values()
print(v)
my_item = student_dict.items()
print(my_item)

# Iterate key and value 
for k, v in student_dict.items(): 
    print("Student key is: ", k, "His/her is: ", v)